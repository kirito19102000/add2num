package main

import (
	"fmt"
	"strconv"
)

func main() {
	addNumbers("456789", "999211")
}

func addNumbers(x, y string) {
	var length = 0
	var num1 = x
	var num2 = y
	var count = 0
	var sum = ""
	var j = 1
	var reNum = 0
	fmt.Println("Phép toán cộng 2 số", num1, "và", num2, ":")

	if len(num1) > len(num2) {
		length = len(num1)
		i := 0
		for len(num2) < length {
			num2 = "0" + num2
			i++
		}
	} else if len(num2) > len(num1) {
		length = len(num2)
		i := 0
		for len(num1) < length {
			num1 = "0" + num1
			i++
		}
	} else {
		length = len(num2)
	}

	for length > 0 {
		fmt.Print("Bước ", j, ",")

		intVar1, err1 := strconv.Atoi(string(num1[length-1]))
		intVar2, err2 := strconv.Atoi(string(num2[length-1]))
		_ = err1
		_ = err2

		if count == 1 {
			if length == 1 {
				sum = strconv.Itoa(intVar1+intVar2+1) + sum
				fmt.Print(" lấy ", intVar1, " cộng ", intVar2, " cộng ", reNum, " nhớ được ", intVar1+intVar2+1, ", lấy ", (intVar1 + intVar2 + 1), " được ", sum)
			} else {
				sum = strconv.Itoa((intVar1+intVar2+1)%10) + sum
				fmt.Print(" lấy ", intVar1, " cộng ", intVar2, " cộng ", reNum, " nhớ được ", intVar1+intVar2+1, ", lấy ", (intVar1+intVar2+1)%10, " nhớ 1 được ", sum)
			}

			reNum = (intVar1 + intVar2 + 1) / 10
			if (intVar1 + intVar2 + 1) >= 10 {
				count = 1
			} else {
				count = 0
			}
		} else if count == 0 {
			sum = strconv.Itoa((intVar1+intVar2)%10) + sum

			fmt.Print(" lấy ", intVar1, " cộng ", intVar2, " cộng ", reNum, " nhớ được ", intVar1+intVar2, ", lấy ", (intVar1+intVar2)%10, " được ", sum)
			reNum = (intVar1 + intVar2) / 10
			if (intVar1 + intVar2) >= 10 {
				count = 1
			} else {
				count = 0
			}
		}

		fmt.Println()
		j++
		length--

	}

}
